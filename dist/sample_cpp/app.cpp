/**
 ******************************************************************************
 ** ファイル名 : app.cpp
 **
 ** 概要 : 二輪差動型ライントレースロボットのTOPPERS/HRP3用Cサンプルプログラム
 **
 ** 注記 : sample_c4 (sample_c3にBluetooth通信リモートスタート機能を追加)
 ******************************************************************************
 **/

#include "ev3api.h"
#include "app.hpp"
#include "etroboc_ext.h"

#if defined(BUILD_MODULE)
    #include "module_cfg.h"
#else
    #include "kernel_cfg.h"
#endif

#define DEBUG

#if defined(DEBUG)
    #define _debug(x) (x)
#else
    #define _debug(x)
#endif

#if defined(MAKE_BT_DISABLE)
    static const int _bt_enabled = 0;
#else
    static const int _bt_enabled = 1;
#endif

/**
 * シミュレータかどうかの定数を定義します
 */
#if defined(MAKE_SIM)
    static const int _SIM = 1;
#elif defined(MAKE_EV3)
    static const int _SIM = 0;
#else
    static const int _SIM = 0;
#endif

/**
 * 左コース/右コース向けの設定を定義します
 * デフォルトは左コース(ラインの右エッジをトレース)です
 */
#if defined(MAKE_RIGHT)
    static const int _LEFT = 0;
    #define _EDGE -1
#else
    static const int _LEFT = 1;
    #define _EDGE 1
#endif

/**
 * センサー、モーターの接続を定義します
 */
static const sensor_port_t
    touch_sensor    = EV3_PORT_1,
    color_sensor    = EV3_PORT_2,
    sonar_sensor    = EV3_PORT_3,
    gyro_sensor     = EV3_PORT_4;

static const motor_port_t
    left_motor      = EV3_PORT_C,
    right_motor     = EV3_PORT_B,
    arm_motor       = EV3_PORT_A,
    tail_motor      = EV3_PORT_D;

static int      bt_cmd = 0;     /* Bluetoothコマンド 1:リモートスタート */
static FILE     *bt = NULL;     /* Bluetoothファイルハンドル */

/* 下記のマクロは個体/環境に合わせて変更する必要があります */
/* sample_c1マクロ */
#define LIGHT_WHITE  23         /* 白色の光センサ値 */
#define LIGHT_BLACK  0          /* 黒色の光センサ値 */
/* sample_c2マクロ */
#define SONAR_ALERT_DISTANCE 30 /* 超音波センサによる障害物検知距離[cm] */
/* sample_c4マクロ */
//#define DEVICE_NAME     "ET0"  /* Bluetooth名 sdcard:\ev3rt\etc\rc.conf.ini LocalNameで設定 */
//#define PASS_KEY        "1234" /* パスキー    sdcard:\ev3rt\etc\rc.conf.ini PinCodeで設定 */
#define CMD_START         '1'    /* リモートスタートコマンド */

/* LCDフォントサイズ */
#define CALIB_FONT (EV3_FONT_SMALL)
#define CALIB_FONT_WIDTH (6/*TODO: magic number*/)
#define CALIB_FONT_HEIGHT (8/*TODO: magic number*/)

/* 関数プロトタイプ宣言 */
static int sonar_alert(void);
static void _syslog(int level, char* text);
static void _log(char* text);
//static void tail_control(signed int angle);
//static void backlash_cancel(signed char lpwm, signed char rpwm, int32_t *lenc, int32_t *renc);
static void tail_open(motor_port_t tail_motor, int target_degree);
static void tail_close(motor_port_t tail_motor, int target_degree);
static void arm_up(motor_port_t arm_motor, int target_degree);
static void arm_down(motor_port_t arm_motor, int target_degree);

#define DELTA_T 0.004
#define KP 7.0
#define KI 0.0
#define KD 5.0
#define M_PI 3.141592653589793
static double diff[2];
static double integral;
static double pre_error = 0.0;
static double pre_pre_error = 0.0;

/* メインタスク */
void main_task(intptr_t unused)
{
    signed char forward;      /* 前後進命令 */
    signed char turn;         /* 旋回命令 */
    //signed char pwm_L, pwm_R; /* 左右モーターPWM出力 */

    /* LCD画面表示 */
    ev3_lcd_fill_rect(0, 0, EV3_LCD_WIDTH, EV3_LCD_HEIGHT, EV3_LCD_WHITE);

    _log("HackEV sample_c4");
    if (_LEFT)  _log("Left course:");
    else        _log("Right course:");

    /* センサー入力ポートの設定 */
    ev3_sensor_config(sonar_sensor, ULTRASONIC_SENSOR);
    ev3_sensor_config(color_sensor, COLOR_SENSOR);
    ev3_sensor_config(gyro_sensor, GYRO_SENSOR);
    ev3_color_sensor_get_reflect(color_sensor); /* 反射率モード */
    ev3_sensor_config(touch_sensor, TOUCH_SENSOR);
    /* モーター出力ポートの設定 */
    ev3_motor_config(left_motor, LARGE_MOTOR);
    ev3_motor_config(right_motor, LARGE_MOTOR);
    ev3_motor_config(arm_motor, LARGE_MOTOR);
    ev3_motor_config(tail_motor, MEDIUM_MOTOR);

    if (_bt_enabled)
    {
        /* Open Bluetooth file */
        bt = ev3_serial_open_file(EV3_SERIAL_BT);
        assert(bt != NULL);

        /* Bluetooth通信タスクの起動 */
        act_tsk(BT_TASK);
    }

    ev3_led_set_color(LED_ORANGE); /* 初期化完了通知 */

    _log("Go to the start, ready?");
    if (_SIM)   _log("Hit SPACE bar to start");
    else        _log("Tap Touch Sensor to start");

    if (_bt_enabled)
    {
        fprintf(bt, "Bluetooth Remote Start: Ready.\n", EV3_SERIAL_BT);
        fprintf(bt, "send '1' to start\n", EV3_SERIAL_BT);
    }

    /* スタート待機 */
    while(1)
    {
        //tail_control(TAIL_ANGLE_STAND_UP); /* 完全停止用角度に制御 */

        if (bt_cmd == 1)
        {
            break; /* リモートスタート */
        }

        if (ev3_touch_sensor_is_pressed(touch_sensor) == 1)
        {
            break; /* タッチセンサが押された */
        }

        tslp_tsk(10 * 1000U); /* 10msecウェイト */
    }

    /* 走行モーターエンコーダーリセット */
    ev3_motor_reset_counts(left_motor);
    ev3_motor_reset_counts(right_motor);
    ev3_motor_reset_counts(arm_motor);
    ev3_motor_reset_counts(tail_motor);
    
    /* 姿勢調整 */
    tail_open(tail_motor, 20);
    tail_close(tail_motor, 20);
    arm_up(arm_motor, 10);
    arm_down(arm_motor, 5);

    /* ジャイロセンサーリセット */
    ev3_gyro_sensor_reset(gyro_sensor);

    ev3_led_set_color(LED_GREEN); /* スタート通知 */

    /**
    * Main loop
    */
    while(1)
    {
        if (ev3_button_is_pressed(BACK_BUTTON)) break;

        // if (sonar_alert() == 1) /* 障害物検知 */
        // {
        //     forward = turn = 0; /* 障害物を検知したら停止 */
        // }
        // else
        // {
        //     forward = 30; /* 前進命令 */
        //     if (ev3_color_sensor_get_reflect(color_sensor) >= (LIGHT_WHITE + LIGHT_BLACK)/2)
        //     {
        //         turn = -80 * _EDGE; /* 右旋回命令　(右コースは逆) */
        //     }
        //     else
        //     {
        //         turn =  80 * _EDGE; /* 左旋回命令　(右コースは逆) */
        //     }
        // }

        forward = 70; /* 前進命令 */

        /* PID Control */
        double error, delta_error1, delta_error2;
        double p, i, d;
        double sensor_val = ev3_color_sensor_get_reflect(color_sensor);
        const int N = 5;
        static double error_array[5] = {}; // 5→Nにするとコンパイルエラー（#^ω^）

        rgb_raw_t rgb_val;
        ev3_color_sensor_get_rgb_raw(color_sensor, &rgb_val);

        //printf("================");
        //printf("%f\n", sensor_val);
        // printf("%d\n", (int)rgb_val.r);
        // printf("%d\n", (int)rgb_val.g);
        // printf("%d\n", (int)rgb_val.b);

        // 青色でライントレース
        //sensor_val = rgb_val.r;

        //double target_val = (LIGHT_WHITE + LIGHT_BLACK)/2;
        double target_val = 20;

        // diff[0] = diff[1];
        // diff[1] = sensor_val - target_val;
        // integral += (diff[1] + diff[0]) / 2.0 * DELTA_T;

        // p = KP * diff[1];
        // i = KI * integral;
        // d = KD * (diff[1]- diff[0]) / DELTA_T;

        // double pid = p + i + d;

        error = target_val - sensor_val;
        delta_error1 = error - pre_error;
        delta_error2 = pre_error - pre_pre_error;
        
        p = KP * error;
        d = KD * (delta_error1 - delta_error2);
        
        i = 0;
        for (int n=0;n<(N-1);++n){
            error_array[n] = error_array[n+1];
            i += error_array[n];
            //printf("%d : %f\n", n, error_array[n]);
        }
        error_array[N-1] = error;
        i += error;
        
        i = KI * i;

        pre_pre_error = pre_error;
        pre_error = error;
        
        int pid = p + i + d;

        if (pid < -100.0)
            pid = -100.0;
        else if (pid > 100.0)
            pid = 100.0;

        turn = -pid * _EDGE;

        // printf("%f, %f, %f, %f, %f\n",error, delta_error1, pre_error, i, turn);
        // printf("%f, %f, %f, %f\n", p, i, d, turn);


        /* 左右モータでロボットのステアリング操作を行う */
        ev3_motor_steer(
            left_motor,
            right_motor,
            (int)forward,
            (int)turn
        );

        tslp_tsk(4 * 1000U); /* 4msec周期起動 */
    }
    ev3_motor_stop(left_motor, false);
    ev3_motor_stop(right_motor, false);

    if (_bt_enabled)
    {
        ter_tsk(BT_TASK);
        fclose(bt);
    }

    ext_tsk();
}

//*****************************************************************************
// 関数名 : sonar_alert
// 引数 : 無し
// 返り値 : 1(障害物あり)/0(障害物無し)
// 概要 : 超音波センサによる障害物検知
//*****************************************************************************
static int sonar_alert(void)
{
    static unsigned int counter = 0;
    static int alert = 0;

    signed int distance;

    if (++counter == 40/4) /* 約40msec周期毎に障害物検知  */
    {
        /*
         * 超音波センサによる距離測定周期は、超音波の減衰特性に依存します。
         * NXTの場合は、40msec周期程度が経験上の最短測定周期です。
         * EV3の場合は、要確認
         */
        distance = ev3_ultrasonic_sensor_get_distance(sonar_sensor);
        if ((distance <= SONAR_ALERT_DISTANCE) && (distance >= 0))
        {
            alert = 1; /* 障害物を検知 */
        }
        else
        {
            alert = 0; /* 障害物無し */
        }
        counter = 0;
    }

    return alert;
}

//*****************************************************************************
// 関数名 : bt_task
// 引数 : unused
// 返り値 : なし
// 概要 : Bluetooth通信によるリモートスタート。 Tera Termなどのターミナルソフトから、
//       ASCIIコードで1を送信すると、リモートスタートする。
//*****************************************************************************
void bt_task(intptr_t unused)
{
    while(1)
    {
        if (_bt_enabled)
        {
            uint8_t c = fgetc(bt); /* 受信 */
            switch(c)
            {
            case '1':
                bt_cmd = 1;
                break;
            default:
                break;
            }
            fputc(c, bt); /* エコーバック */
        }
    }
}

//*****************************************************************************
// 関数名 : _syslog
// 引数 :   int   level - SYSLOGレベル
//          char* text  - 出力文字列
// 返り値 : なし
// 概要 : SYSLOGレベルlebelのログメッセージtextを出力します。
//        SYSLOGレベルはRFC3164のレベル名をそのまま（ERRだけはERROR）
//        `LOG_WARNING`の様に定数で指定できます。
//*****************************************************************************
static void _syslog(int level, char* text){
    static int _log_line = 0;
    if (_SIM)
    {
        syslog(level, text);
    }
    else
    {
        ev3_lcd_draw_string(text, 0, CALIB_FONT_HEIGHT*_log_line++);
    }
}

//*****************************************************************************
// 関数名 : _log
// 引数 :   char* text  - 出力文字列
// 返り値 : なし
// 概要 : SYSLOGレベルNOTICEのログメッセージtextを出力します。
//*****************************************************************************
static void _log(char *text){
    _syslog(LOG_NOTICE, text);
}

//*****************************************************************************
// 関数名 : tail_oepn
// 引数 :   motor_port_t tail_motor  - モータ
//          int oepration_degree     - 操作角度
// 返り値 : なし
// 概要 : しっぽの角度を変更します。
//*****************************************************************************
static void tail_open(motor_port_t tail_motor, int oepration_degree){
    /* しっぽを指定した角度だけひろげる */
    int power = 50; 
    int tail_current_degree = ev3_motor_get_counts(tail_motor); //初期値0[deg]位
    printf("now tail : %d [deg]\n", tail_current_degree);
    printf("operation : %d [deg]\n", oepration_degree);
    ev3_motor_set_power(tail_motor, power);
    int tail_target_degree = tail_current_degree + oepration_degree;
    int deg_tail_motor;
    while (true)
    {
        deg_tail_motor = ev3_motor_get_counts(tail_motor);
        if(deg_tail_motor > tail_target_degree)
        {
            ev3_motor_stop(tail_motor, true);
            break;
        }else
        {
            // printf("tail : %d [deg]\n", deg_tail_motor);
        }
    }
    printf("tail is ready : %d [deg]\n", deg_tail_motor);
    printf("\n");
}

//*****************************************************************************
// 関数名 : tail_close
// 引数 :   motor_port_t tail_motor  - モータ
//          int oepration_degree     - 操作角度
// 返り値 : なし
// 概要 : しっぽの角度を変更します。
//*****************************************************************************
static void tail_close(motor_port_t tail_motor, int oepration_degree){
    /* しっぽを指定した角度だけたたむ */
    int power = -50; 
    int tail_current_degree = ev3_motor_get_counts(tail_motor); //初期値0[deg]位
    printf("now tail : %d [deg]\n", tail_current_degree);
    printf("operation : %d [deg]\n", oepration_degree);
    ev3_motor_set_power(tail_motor, power);
    int tail_target_degree = tail_current_degree - oepration_degree;
    int deg_tail_motor;
    while (true)
    {
        deg_tail_motor = ev3_motor_get_counts(tail_motor);
        if(deg_tail_motor < tail_target_degree)
        {
            ev3_motor_stop(tail_motor, true);
            break;
        }else
        {
            // printf("tail : %d [deg]\n", deg_tail_motor);
        }
    }
    printf("tail is ready : %d [deg]\n", deg_tail_motor);
    printf("\n");
}

//*****************************************************************************
// 関数名 : arm_up
// 引数 :   motor_port_t arm_motor    モータ
//          int oepration_degre     - 操作角度
// 返り値 : なし
// 概要 : アームの角度を変更します。
//*****************************************************************************
static void arm_up(motor_port_t arm_motor, int oepration_degree){
    /* アームを指定した角度だけあげる */ 
    int power = 50; 
    int arm_current_degree = ev3_motor_get_counts(arm_motor); //初期値-60[deg]位
    printf("now arm : %d [deg]\n", arm_current_degree);
    printf("operation : %d [deg]\n", oepration_degree);
    ev3_motor_set_power(arm_motor, power);
    int arm_target_degree = arm_current_degree + oepration_degree;
    int deg_arm_motor;
    while (true)
    {
        deg_arm_motor = ev3_motor_get_counts(arm_motor);
        if(deg_arm_motor > arm_target_degree)
        {
            ev3_motor_stop(arm_motor, true);
            break;
        }else
        {
            // printf("arm : %d [deg]\n", deg_arm_motor);
        }
    }
    printf("arm is ready : %d [deg]\n", deg_arm_motor);
    printf("\n");
}

//*****************************************************************************
// 関数名 : arm_down
// 引数 :   motor_port_t arm_motor    モータ
//          int oepration_degre     - 操作角度
// 返り値 : なし
// 概要 : アームの角度を変更します。
//*****************************************************************************
static void arm_down(motor_port_t arm_motor, int oepration_degree){
    /* アームを指定した角度だけ下げる */
    int power = -50; 
    int arm_current_degree = ev3_motor_get_counts(arm_motor); //初期値-60[deg]位
    printf("now arm : %d [deg]\n", arm_current_degree);
    printf("operation : %d [deg]\n", oepration_degree);
    ev3_motor_set_power(arm_motor, power);
    int arm_target_degree = arm_current_degree - oepration_degree;
    int deg_arm_motor;
    while (true)
    {
        deg_arm_motor = ev3_motor_get_counts(arm_motor);
        if(deg_arm_motor < arm_target_degree)
        {
            ev3_motor_stop(arm_motor, true);
            break;
        }else
        {
            // printf("arm : %d [deg]\n", deg_arm_motor);
        }
    }
    printf("arm is ready : %d [deg]\n", deg_arm_motor);
    printf("\n");
}
